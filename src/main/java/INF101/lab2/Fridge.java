package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    
    int max_size = 20;
    ArrayList<FridgeItem> container  = new ArrayList<FridgeItem>();
    
    

    @Override
    public int nItemsInFridge() {
        return container.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(container.size() < totalSize()){
            container.add(item);
            return true;
            
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(container.contains(item)){
            container.remove(item);
            return;
        } 
        throw new NoSuchElementException();
    }

    @Override
    public void emptyFridge() {
        container.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {

        ArrayList<FridgeItem> expiredItems = new ArrayList<FridgeItem>();

        for(FridgeItem toCheck : container){
            if(toCheck.hasExpired()){
                expiredItems.add(toCheck);
            }
        }
        for(FridgeItem toCheck : expiredItems){
            container.remove(toCheck);
        }

        return expiredItems;
    }
}